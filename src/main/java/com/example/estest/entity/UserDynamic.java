package com.example.estest.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author whx
 * @date 2022/7/2
 */
@Data
@Document(indexName = "user-dynamic-"+"#{indexNameBean.active}",replicas = 0,shards = 1, type = "_doc")
public class UserDynamic implements Serializable {

    @Id
    private String id;

    @Field(type = FieldType.Text, name = "name",analyzer = "standard")
    private String name;

    @Field(type = FieldType.Keyword, name = "password")
    private String password;

    @Field(type = FieldType.Integer, name = "sex")
    private Integer sex;

    @Field(type = FieldType.Text, name = "address", analyzer = "ik_smart")
    private String address;

    @Field(type = FieldType.Date, name = "create_date", format = DateFormat.custom, pattern="yyyy-MM-dd HH:mm:ss||strict_date_optional_time||epoch_millis")
    private Date createDate;

    @Field(type = FieldType.Long, name = "role_ids")
    private List<Long> roleIds;

    @Field(type = FieldType.Nested, name = "department_list")
    private List<Department> departmentList;

}
