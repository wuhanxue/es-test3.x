package com.example.estest.repository;

import com.example.estest.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import java.util.List;

/**
 * @author whx
 * @date 2022/7/2
 */
public interface UserRepository extends ElasticsearchCrudRepository<User,String> {

    User findByName(String name);

    List<User> findAllByName(String name);

    Page<User> findAllByName(String name, Pageable pageable);

    @Query("{\"term\": {\"name\": {\"value\": \"?0\"}}}")
    User queryByName(String name);


}
