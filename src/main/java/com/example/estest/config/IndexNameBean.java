package com.example.estest.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author whx
 * @date 2022/7/22
 */
@Component
@Getter
public class IndexNameBean {

    @Value("${spring.profiles.active}")
    private String active;
}
